package Ejercicios2;


/**
 * Ejercicio 3: esqueleto para el programa de fechas y turnos.
 *
 * @author merce
 */
public class Ejercicio03 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //----------------------------------------------
        //          Declaración de variables 
        //----------------------------------------------

        
        //----------------------------------------------
        //                Entrada de datos 
        //----------------------------------------------
        System.out.println("ASIGNANDO TURNOS A FECHAS");
        System.out.println("-------------------------");

        // 1. Creación un objeto de la clase LocalDate con fecha de inicio de curso 
        // Fecha de inicio del curso

        // 2. Creación un objeto de la clase LocalDate con fecha de fin de curso 
        // Fecha de fin de curso

        // 3. Lectura por teclado y comprobación de año, mes y día

        // 3.1. Leer y comprobar del año (debe ser 2020 o bien 2021)

        // 3.2.  Leer y comprobar el mes (Lógicamente entre 1 y 12)

        // 3.3.  Leer y comprobar el día

        // 4. Creación de objeto LocalDate (si es posible) 
        // a partir de esos datos de fecha
        // Fecha leída (Saltará una excepción DateTimeException si por
        // ejemplo se intenta crear una fecha con 31 de febrero)

        //----------------------------------------------
        //      Procesamiento + Salida de resultados 
        //----------------------------------------------

        // 5. Comprobar si la fecha está dentro del período escolar, si no
        // lo está se informará y no haremos nada más
            
            
        // 6. Si es sábado o domingo, no procede calcular nada 

            
        // 7. Si la fecha es apropiada (correcta y no fin de semana)
        // se muestra por pantalla cuándo debe ir cada alumno a clase:
        // turno de mañana o de tarde
    }
}
