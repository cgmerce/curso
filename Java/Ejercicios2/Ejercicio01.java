package Ejercicios2;

import libtarea3.ComeCocos;
import libtarea3.Direccion;



/**
 * Ejercicio 1: esqueleto para escribir las instrucciones del comecocos
 * 
 * @author merce
 */
public class Ejercicio01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //----------------------------------------------
        //          Declaración de variables 
        //----------------------------------------------
        // 1. Declarar una variable referencia a objetos instancia de la clase ComeCocos
        // (variable miComeCocos).
        
        ComeCocos MiComeCocos;

        //----------------------------------------------
        //                Entrada de datos 
        //----------------------------------------------
        System.out.println("MOVIENDO LOS COMECOCOS");
        System.out.println("----------------------");
        // 2. Instanciar un objeto de la clase ComeCocos ubicado en la posición x=120, y=215 
        // y mirando hacia el ESTE.
        // Asignar a la variable anterior la referencia al objeto recién creado.        
        
        MiComeCocos = new ComeCocos(120, 215, Direccion.ESTE);

        //----------------------------------------------
        //       Procesamiento + Salida de Resultados
        //----------------------------------------------
        // 3. Mostrar por pantalla el estado  inicial del objeto.
        // Dispones para ello del método toString.
        
        System.out.println(MiComeCocos.toString());
        
        // 4. Hacer avanzar 10 posiciones al comecocos.
        
        MiComeCocos.avanzar(10);
               
        // 5. Mostrar por pantalla el estado del comecocos.
        
        System.out.println(MiComeCocos.toString());
        
        // 6. Girar hacia la derecha el comecocos.
        
        MiComeCocos.giraDerecha();
        
        // 7. Hacer avanzar otras 10 posiciones al comecocos.

        MiComeCocos.avanzar(10);
        
        // 8. Mostrar por pantalla el estado del comecocos.

        System.out.println(MiComeCocos.toString());
        
        // 9. Hacer avanzar 25 posiciones al comecocos.
        
        MiComeCocos.avanzar(25);
        
        // 10. Mostrar por pantalla el estado del comecocos.

        System.out.println(MiComeCocos.toString());
        
        // 11. Girar hacia la izquierda el comecocos.

        MiComeCocos.giraIzquierda();
        
        //12. Volver a girar hacia la izquierda el comecocos.

        MiComeCocos.giraIzquierda();
        
        // 13. Hacer avanzar 20 posiciones al comecocos.

        MiComeCocos.avanzar(20);
        
        // 14. Mostrar por pantalla el estado del comecocos.

        System.out.println(MiComeCocos.toString());
        
        // 15. Hacer avanzar 45 posiciones al comecocos.

        MiComeCocos.avanzar(45);
        
        // 16. Mostrar por pantalla el estado del comecocos.

        System.out.println(MiComeCocos.toString());
        
        // 17. Obtener la dirección a la que está mirando el comecocos y 
        // convertirla a grados con la herramienta que proporciona la clase 
        // (método estático direccionAGrados).
        System.out.print("La dirección actual del ComeCocos es: ");
        System.out.print(ComeCocos.direccionAGrados(MiComeCocos.getDireccion()));
        System.out.println(" grados.");
        
        System.out.println("\n\nFin de impresión.");
    }
    
}
