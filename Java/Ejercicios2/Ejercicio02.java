package Ejercicios2;

/**
 * Ejercicio 2: esqueleto para escribir las instrucciones.
 * 
 * @author merce
 */
public class Ejercicio02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //----------------------------------------------
        //          Declaración de variables 
        //----------------------------------------------

        
        
        //----------------------------------------------
        //                Entrada de datos 
        //----------------------------------------------
        System.out.println("LANZANDO LOS DADOS");
        System.out.println("------------------");
        // No se requiere que el usuario introduzca ningún dato
        
        // 1. Pruebas del constructor
        // Se intentan crear 21 dados: desde 0 caras hasta 20 caras (bucle)
        // Sólo algunas llamadas al constructor funcionarán
        // Otras harán que salte una excepcíon IllegalArgumentException
        // Habrá que recogerla y mostrar el mensaje de error por pantalla
        
        // 2. Creación de un dado de 6 caras 
        // Esta instanciación no debería dar problema 
        // (no es necesario encerrarla en un try-catch)

        //----------------------------------------------
        //      Procesamiento + Salida de resultados 
        //----------------------------------------------

        // 3. Pruebas de lanzamiento: primer lanzamiento
                
        // 4. Pruebas de lanzamiento: otros 9 lanzamientos (bucle que se realiza nueve veces)

        // 5. Análisis de resultados
        
        // 5.1. Número total de lanzamientos

        // 5.2. Análisis de los lanzamientos realizados (bucle desde 1 hasta el número de caras)   

    }
}

