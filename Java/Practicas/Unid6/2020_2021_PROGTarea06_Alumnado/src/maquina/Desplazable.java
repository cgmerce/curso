package maquina;

public interface Desplazable {

    double getKilometrosSinRepostar();

    double getTotalKilometrosRecorridos();

    void desplazar(double kilometros);
    
}
