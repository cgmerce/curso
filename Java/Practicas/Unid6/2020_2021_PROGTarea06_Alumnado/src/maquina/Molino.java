
package maquina;

public class Molino extends MaquinaMecanica {

    TipoMolino tipoMolino;

    public Molino(String marca, String modelo, Fuerza fuerzaMotriz) {        
        super(marca, modelo, fuerzaMotriz);  //super (de la clase padre), lo primero que se llama.
    /*Este constructor asignará el valor adecuado al parámetro tipoMolino dependiendo de 
    *cuál sea la fuerza motriz indicada como parámetro. 
    *También invocará al constructor de la superclase, y por tanto lanzará una
    *NullPointerException si el argumento recibido como fuerza motriz es null
    *(definido en clase MaquinaMecanica)*/
        switch (fuerzaMotriz) {
            case ANIMAL:
                tipoMolino = TipoMolino.FUERZA_ANIMAL;
                break;
            case ELECTRICIDAD:
                tipoMolino = TipoMolino.ELECTRICO;
                break;
            case COMBUSTIBLE:
                tipoMolino = TipoMolino.A_MOTOR_COMBUSTION;
                break;
            case CORRIENTE_AGUA:
                tipoMolino = TipoMolino.DE_AGUA;
                break;
            case VIENTO:
                tipoMolino = TipoMolino.DE_VIENTO;
                break;
            default:
                throw new IllegalArgumentException("Error. Fuerza motriz no apta para usar con molinos.");                
        }
    }
       
          
       

    public TipoMolino getTipoDeMolino() {
        return tipoMolino;
    }

    @Override
    public String toString() {
        return "{Marca: " + marca + "; Modelo: " + modelo + "; NS: " + numeroSerie
                + "; FuerzaMotriz: " + fuerzaMotriz + "; Molino de: " + tipoMolino + ".}" ;
    }
    
    
    
}


//