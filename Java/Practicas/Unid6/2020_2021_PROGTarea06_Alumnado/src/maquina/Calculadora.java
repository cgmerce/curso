/*
 * La clase Calculadora representa un tipo especial de máquina eléctrica con
 * capacidad para ser recargada, pero no enchufada, ya que funciona te
 * exclusivamenente con pilas. 
 */
package maquina;

/**
 *
 * @author
 */
public class Calculadora extends MaquinaElectrica implements Recargable {

    //Se añaden los siguientes atributos.
    TipoPila tipoPila;
    double horasDeUso;
    boolean pilaAgotada = true;
    static final double HORAS_DE_USO = 100.0;
    public static final TipoPila DEFAULT_TIPO_PILA = TipoPila.AA_1_5V;
    private final Boolean DEFAULT_PILA_AGOTADA = true;

    /**
     * Implementar un Constructor que reciba cuatro parámetros. Aprovecha el
     * otro constructor para asignar el valor de los parámetros a los atributos
     * correspondientes, y actualizar el valor del atributo pilaAgotada con el
     * valor por defecto indicado por la correspondiente constante.
     *
     * @param marca
     * @param modelo
     * @param tipoPila
     * @param estadoDePila
     */
    public Calculadora(String marca, String modelo, TipoPila tipoPila, boolean estadoDePila) {
        this(marca, modelo, tipoPila);
        this.pilaAgotada = estadoDePila;
    }

    /**
     * Implementar un Constructor que reciba tres parámetros.
     *
     * @param marca
     * @param modelo
     * @param tipoPila
     */
    public Calculadora(String marca, String modelo, TipoPila tipoPila) {
        super(marca, modelo);
        if (tipoPila == null) {
            throw new NullPointerException("Error en tipo pila: null. El tipo de pila no puede ser nulo.");
        } else if ((tipoPila.compareTo(TipoPila.AA_1_5V) == 0) || (tipoPila.compareTo(TipoPila.BOTON_CR1025_3V) == 0)) {
            throw new IllegalArgumentException("Error en tipo de pila: " + tipoPila.toString() + ". Las calculadoras solo admiten pilas de tipo" + TipoPila.AAA_1_5V.toString() + " y " + TipoPila.AA_1_5V.toString() + ".");
        } else {
            this.tipoPila = tipoPila;
            this.pilaAgotada = this.DEFAULT_PILA_AGOTADA;
            this.horasDeUso = 0;
        }
    }

    /**
     * Permite cargar una calculadora. Al tratarse de calculadoras solo de
     * pilas, la operación consistirá en reponer las pilas usadas con pilas
     * nuevas, actualizando convenientemente el estado de la pila, asignando el
     * valor false al atributo pilaAgotada y reiniciando las horas de uso a 0.
     */
    @Override
    public void cargar() {
        this.horasDeUso = 0;
        this.pilaAgotada = false;
    }

    /**
     * Método que devuelve el número de horas de uso restantes para la
     * calculadora, tras haber sido usada el número de horas que se recibe como
     * parámetro. Se estima que todas las calculadoras vienen equipadas con las
     * pilas necesarias para un mismo número de horas de funcionamiento,
     * expresado por la constante de clase HORAS_DE_USO. Las horas restantes se
     * calcularán restando a la constante HORAS_DE_USO el valor del atributo
     * horasDeUso de la calculadora concreta que invoca al método. Si la pila no
     * está ya agotada, y las horas de uso que se quieren aplicar son menores
     * que las horas restantes, le sumaremos las horas al total de horas de uso
     * de la calculadora, y se las restaremos a las horas restantes. En caso
     * contrario, actualizaremos el atributo pilaAgotada para que refleje que se
     * ha agotado, y las horas restantes las pondremos a 0. El método devolverá
     * como resultado el valor para las horas restantes que haya resultado.
     *
     * @param cantidad
     * @return
     */
    @Override
    public double usarBateria(double cantidad) {
        double horasRestantes = 0.0;
        horasRestantes = (HORAS_DE_USO - this.horasDeUso);
        if (!pilaAgotada && (horasRestantes > cantidad)) {
            this.horasDeUso += cantidad;
            horasRestantes -= cantidad;
        } else {
            this.pilaAgotada = true;
            horasRestantes = 0.0;
        }
        return horasRestantes;
    }

    /**
     * Se basará en el resultado obtenido por el método toString de la
     * superclase, añadirle el tipo de pila y las horas de uso restantes,
     * reutilizando todo el código que se pueda de la clase padre y de otros
     * métodos de esta misma clase. El resultado debe ser algo del tipo: {
     * Marca: XXX; modelo: YYY; NS: ZZZ; Voltaje: WWW v.; Potencia: VVVV W; Tipo
     * de pila: UUU; Horas de uso restantes: RRR} donde XXX representa la marca,
     * YYY representa el modelo, ZZZ representa el número de serie, WWW
     * representa el voltaje, VVV representa la potencia eléctrica, UUU
     * representa el tipo de pila usado y RRR representa las horas de uso
     * restantes.
     *
     * @return
     */
    @Override
    public String toString() {
        String ret = super.toString().replace("}", "");
        String hRest = String.valueOf(this.usarBateria(0.0));
        StringBuilder sb = new StringBuilder();
        sb.append(ret);
        sb.append("; Tipo de pila: ").append(tipoPila.toString());
        sb.append("; Horas de uso restantes: ").append(hRest);
        sb.append('}');
        return sb.toString();
    }

}
