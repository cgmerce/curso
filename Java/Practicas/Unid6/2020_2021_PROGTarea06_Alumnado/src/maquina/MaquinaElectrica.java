package maquina;

public abstract class MaquinaElectrica extends Maquina {

    int voltaje;
    double potenciaElectrica;
    static final int MIN_VOLTAJE = 10;
    static final int MAX_VOLTAJE = 400;
    static int DEFAULT_VOLTAJE = MIN_VOLTAJE;
    static final double MIN_POTENCIA_ELECTRICA = 700.0;
    static final double MAX_POTENCIA_ELECTRICA = 200000.0;
    static double DEFAULT_POTENCIA = MIN_POTENCIA_ELECTRICA;

    public MaquinaElectrica(String marca, String modelo, int voltaje, double potenciaElectrica) {
        super(marca, modelo);
        this.voltaje = voltaje;
        this.potenciaElectrica = potenciaElectrica;
        // TODO Implementar la excepcion por comprobacion de voltaje etc.

        /*Este constructor podrá lanzar una IllegalArgumentException cuando se
        intente crear un objeto con un voltaje menor que el mínimo o mayor que 
        el máximo (Mensaje asociado: Error de voltaje: XX v.  Mínimo %d YY v.
        y máximo ZZ v. */
        if (voltaje >= MIN_VOLTAJE && voltaje <= MAX_VOLTAJE) {
            this.voltaje = voltaje;
        } else {
            StringBuilder error = new StringBuilder("Error de voltaje: ");
            error.append(voltaje);
            error.append(" v. Mínimo ");
            error.append(MIN_VOLTAJE);
            error.append(" v. y máximo ");
            error.append(MAX_VOLTAJE);
            error.append(" v.");
            throw new IllegalArgumentException(error.toString());
        }

        /* La misma excepción se podrá lanzar en caso de intentar asignar un
        valor fuera de rango a la potencia eléctrica. (Mensaje asociado: Error 
        de potencia:XX. No puede ser menor que YY w. ni superar los ZZ w.*/
        if (potenciaElectrica >= MIN_POTENCIA_ELECTRICA && potenciaElectrica <= MAX_POTENCIA_ELECTRICA) {
            this.potenciaElectrica = potenciaElectrica;
        } else {
            StringBuilder maqelec = new StringBuilder("Error de potencia: ");
            maqelec.append(potenciaElectrica);
            maqelec.append(". No puede ser menor que ");
            maqelec.append(MIN_POTENCIA_ELECTRICA);
            maqelec.append(" w. ni superar los ");
            maqelec.append(MAX_POTENCIA_ELECTRICA);
            maqelec.append(" w.");
            throw new IllegalArgumentException(maqelec.toString());
        }
    }

    public MaquinaElectrica(String marca, String modelo) {
        this(marca, modelo, DEFAULT_VOLTAJE, DEFAULT_POTENCIA);
    }

    public int getVoltaje() {
        return voltaje;
    }

    public double getPotenciaElectrica() {
        return potenciaElectrica;
    }
    static double DEFAULT_POTENCIA_ELECTRICA = MIN_POTENCIA_ELECTRICA;

    @Override
    public String toString() {
        return "{ Marca: " + marca + "; modelo: " + modelo + "; NS: " + numeroSerie + "; Voltaje: " + voltaje + "v.; Potencia: " + potenciaElectrica + "W. }";
    }

}
