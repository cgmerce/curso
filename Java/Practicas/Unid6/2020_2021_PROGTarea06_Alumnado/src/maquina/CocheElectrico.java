/*
 * La clase CocheElectrico representa un tipo especial de coche, con capacidad
 * para desplazarsee, con una batería que se puede enchufar y recargar, y por 
 * tanto, heredará las características de la clase padre Coche, e implementará
 * las interfaces Desplazable, Enchufable y Recargable. 
 * No va a ser posible extender esta clase definiendo nuevas subclases de coches.
 */
package maquina;

import java.util.Arrays;
import javafx.beans.binding.When;

/**
 *
 * @author MMLA
 * @
 */
public final class CocheElectrico extends Coche implements Desplazable, Enchufable, Recargable {

    private final Integer[] valoresVoltaje = {12, 24, 48};
    private final Double[] valoresCapacidadBateria = {35.0, 50.0, 75.0, 100.0, 125.0, 150.0, 200.0};
    static final int DEFAULT_VOLTAJE_BATERIA = 12;
    static final Double DEFAULT_CAPACIDAD_MAXIMA_BATERIA = 100.0;
    static final Double DEFAULT_POTENCIA = 100.000;
    private final Double DEFAULT_CARGA_EFECTIVA = DEFAULT_CAPACIDAD_MAXIMA_BATERIA / 2;
    public static final Double MIN_AUTONOMIA = 300.0;
    public static final Double MAX_AUTONOMIA = 600.0;
    private int voltajeBateria;
    private Double capacidadMaximaBateria;
    private Double cargaEfectiva;
    private Double autonomia;

    /**
     * Array que en cada fila establece los países que comparten un determinado
     * voltaje estándar. Cada fila por tanto, corresponderá a un voltaje
     * estándar diferente. Este listado se usa internamente por la clase, por lo
     * que no es necesario que sea público.
     *
     */
    private final static String[][] LISTA_PAISES_COMPATIBLES = {
        {"España", "Portugal"}, // 12 v.
        {"Francia", "Bélgica"}, // 24 v.
        {"Inglaterra"} // 48 v.
    };

    /**
     * Implementar un Constructor que reciba tres parámetros: marca, modelo y
     * fuerza motriz del coche eléctrico. Si la fuerza motriz es null, o si toma
     * un valor diferente a ELECTRICIDAD (del enumerado Fuerza), se lanzará una
     * IllegalArgumentException con el mensaje: Error en fuerza Motriz: XX. Para
     * un coche eléctrico debe ser necesariamente YY. , donde XX es el valor
     * erróneo que se ha intentado asignar, e YY el valor que debe tener
     * necesariamente.
     *
     * @param marca
     * @param modelo
     * @param fuerzaMotriz
     */
    public CocheElectrico(String marca, String modelo, Fuerza fuerzaMotriz) {
        this(marca, modelo, fuerzaMotriz, DEFAULT_VOLTAJE_BATERIA, DEFAULT_CAPACIDAD_MAXIMA_BATERIA, MIN_AUTONOMIA);
    }

    //Implementar un Constructor que reciba 6 parámetros.
    public CocheElectrico(String marca, String modelo, Fuerza fuerzaMotriz,
            int voltajeBateria, double capacidadMaximaBateria, double autonomia) {
        super(marca, modelo, fuerzaMotriz);

        if (fuerzaMotriz.compareTo(Fuerza.ELECTRICIDAD) != 0) {
            Maquina.numeroSerie -= 1;
            throw new IllegalArgumentException("Error en fuerza Motriz: "
                    + fuerzaMotriz + ". Para un coche eléctrico debe ser necesariamente "
                    + Fuerza.ELECTRICIDAD);
        }

        if (Arrays.asList(valoresVoltaje).indexOf(voltajeBateria) != -1) {            
            this.voltajeBateria = voltajeBateria;
        } else {
            Maquina.numeroSerie -= 1;
            StringBuilder txt = new StringBuilder();
            txt.append("Error en voltaje: ");
            txt.append(voltajeBateria);
            txt.append(" v. Valores válidos=> 12 v. en España y Portugal, 24 v. en Francia y Bélgica  y 48 v. en Inglaterra.");
            throw new IllegalArgumentException(txt.toString());
        }

        if (Arrays.asList(valoresCapacidadBateria).indexOf(capacidadMaximaBateria) != -1) {
            this.capacidadMaximaBateria = valoresCapacidadBateria[Arrays.asList(valoresCapacidadBateria).indexOf(capacidadMaximaBateria)];
        } else {
            Maquina.numeroSerie -= 1;
            throw new IllegalArgumentException("Valor erróneo en la capacidad máxima de la batería.");
        }

        if (autonomia >= MIN_AUTONOMIA && autonomia <= MAX_AUTONOMIA) {
            this.autonomia = autonomia;
        } else {
            Maquina.numeroSerie -= 1;
            throw new IllegalArgumentException("Error. En el valor de la autonomía.");
        }
    }

    //Implementar el método abstracto getTipoCombustible heredado de la clase Coche. 
    @Override
    public TipoCombustible getTipoCombustible() {
        return this.combustibleUsado;
    }

    /**
     * Redefinir o ampliar el método toString para que devuelva más información
     * de la que devuelve su clase padre (voltaje, capacidad y países
     * compatibles). { Marca: XXX; modelo: YYY; NS: ZZZ; Fuerza Motriz: WWW;
     * Combustible: VVV; Km. sin repostar: UUU; Kilometraje: TTT; Voltaje: OOO;
     * Capacidad batería: PPP; Autonomía: QQQ; Carga efectiva:RRR; Países
     * compatibles: SSS }
     */
    @Override
    public String toString() {
        // TODO Escribir el metodo 
        String info = " Autonomía: " + autonomia + "; Carga efectiva: "
                + cargaEfectiva + "; Países compatibles: " + LISTA_PAISES_COMPATIBLES + "}";
        StringBuilder msg = new StringBuilder(super.toString());
        msg.insert(msg.indexOf("}"), info);
        return msg.toString();
    }

    @Override
    public int getVoltaje() {
        return this.voltajeBateria;
    }

    @Override
    public String[] getPaisesCompatibles() {
        String[] ret;
        ret = LISTA_PAISES_COMPATIBLES[Arrays.asList(valoresVoltaje).indexOf(this.voltajeBateria)];
        return ret;
    }

    @Override
    public void cargar() {
        super.repostar();
    }

    @Override
    public double usarBateria(double cantidad) {
        Double cargaNecesaria;
        cargaNecesaria = cantidad * this.capacidadMaximaBateria / MAX_AUTONOMIA;
        return cargaNecesaria;
    }

    private Exception IllegalArgumentException(IllegalArgumentException IllegalArgumentException) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Por heredar de Coche, ya implementa la interfaz Desplazable a través de
     * la clase padre Coche, pero queremos redefinir el método desplazar(double
     * kilometros).
     *
     * @param kilometros
     */
    @Override
    public void desplazar(double kilometros) {
        double cargaNecesaria = usarBateria(kilometros);
        double porcentajeDeCarga = cargaEfectiva - cargaNecesaria;
        if (porcentajeDeCarga <= 0) {
            throw new IllegalArgumentException("Error: Porcentaje de carga: "
                    + cargaEfectiva + ". No es suficiente para recorrer" + kilometros + ".");
        } else {
            this.kilometrosTotalesRecorridos += kilometros;
            this.kilometrosSinRepostar += kilometros;
            this.cargaEfectiva -= cargaNecesaria;
        }
    }
}
