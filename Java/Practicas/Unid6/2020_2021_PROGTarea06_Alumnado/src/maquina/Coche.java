package maquina;

public abstract class Coche extends MaquinaMecanica implements Desplazable {

    double kilometrosSinRepostar;
    double kilometrosTotalesRecorridos;
    TipoCombustible combustibleUsado;
    static final double MAX_DESPLAZAMIENTO = 1.500;

    public Coche(String marca, String modelo, Fuerza fuerzaMotriz) {
        super(marca, modelo, fuerzaMotriz);
        if ((fuerzaMotriz.compareTo(Fuerza.ELECTRICIDAD) != 0) && (fuerzaMotriz.compareTo(Fuerza.COMBUSTIBLE) != 0)) {
            throw new IllegalArgumentException("Valor de Fuerza motriz incompatible con un Coche: "
                    + fuerzaMotriz + ".");
        }
        this.kilometrosSinRepostar = 0.0;
        this.kilometrosTotalesRecorridos = 0.0;
        if (fuerzaMotriz.compareTo(Fuerza.ELECTRICIDAD) == 0) {
            this.combustibleUsado = TipoCombustible.ELECTRICIDAD;
        }
    }

    /*Implementar un método repostar() que inicialice a cero los kilómetros 
    recorridos sin repostar.*/
    public void repostar() {
        this.kilometrosSinRepostar = 0.0;
    }

    /*Implementar la interfaz Desplazable, lo que obliga a declarar los métodos:
    getKilometrosSinRepostar, getTotalKilometrosRecorridos, desplazar*/
    @Override
    public void desplazar(double kilometros) {
        // El enunciado dice "... máximo desplazamiento permitido sin repostar..."
        // Entiendo que es la cantidad MAX_DESPLAZAMIENTO - kilometrosSinRepostar
        double kmMaxSinRepo = MAX_DESPLAZAMIENTO - this.kilometrosSinRepostar;
        if ((kmMaxSinRepo - kilometros) < 0) {
            throw new IllegalArgumentException("Cantidad de kilómetros negativa, o excesiva para un Coche (Máx: "
                    + kmMaxSinRepo + " .): " + kilometros + ".");
        } else {
            this.kilometrosSinRepostar += kilometros; //this.kilometrosSinRepostar = this.kilometrosSinRepostar + kilometros
            this.kilometrosTotalesRecorridos += kilometros;
        }
    }

    @Override
    public double getKilometrosSinRepostar() {
        return kilometrosSinRepostar;
    }

    @Override
    public double getTotalKilometrosRecorridos() {

        return kilometrosTotalesRecorridos;
    }

    /*Garantizar que cualquier subclase implementará el método getTipocombustible,
    que nos permitirá consultar el tipo de combustible del coche*/
    public abstract TipoCombustible getTipoCombustible();

    // Ampliar método toString para que devuelva más información de la que 
    //devuelve la clase padre.
    @Override
    public String toString() {
        String mas = " Fuerza motriz: " + this.getFuerzaMotriz().toString() + "; Combustible: " + combustibleUsado + "; Km. sin repostar: "
                + kilometrosSinRepostar + "}";
        StringBuilder mesg = new StringBuilder(super.toString());
        mesg.insert(mesg.indexOf("}"), mas);
        return mesg.toString();
    }

}
