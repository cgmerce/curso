
package maquina;


public abstract class Maquina {
    String marca;
    String modelo;
    public static Integer numeroSerie = -1;
    
    static int cantidadDeMaquinasFabricadas;

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public Integer getNumeroSerie() {
        return numeroSerie;
    }

    public static int getCantidadDeMaquinasFabricadas() {
        return cantidadDeMaquinasFabricadas;
    }
    
    @Override
    public String toString() {
        return "{ Marca: " + marca + "; modelo: " + modelo + "; NS: " + numeroSerie + '}';
    }

    public Maquina(String marca, String modelo) {
        this.marca = marca;
        this.modelo = modelo;
        Maquina.numeroSerie++;
    }
    
}
