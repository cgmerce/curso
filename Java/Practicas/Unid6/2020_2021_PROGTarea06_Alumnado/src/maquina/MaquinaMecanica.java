package maquina;

public abstract class MaquinaMecanica extends Maquina {

    Fuerza fuerzaMotriz;
    static final Fuerza DEFAULT_FUERZA_MOTRIZ = Fuerza.COMBUSTIBLE;

    public Fuerza getFuerzaMotriz() {
        return fuerzaMotriz;
    }

    public MaquinaMecanica(String marca, String modelo, Fuerza fuerzaMotriz) {
        super(marca, modelo);
        if (fuerzaMotriz != null) {
            this.fuerzaMotriz = fuerzaMotriz;
        } else {
            Maquina.numeroSerie -= 1;
            throw new NullPointerException("Error: null. La fuerza motriz no puede ser nula, debe indicarse una fuerza motriz válida.");
        }
        if (!contiene(Fuerza.values(), fuerzaMotriz)){
          throw new IllegalArgumentException("Error, fuerza motriz no valida: " + fuerzaMotriz);
        } //si se ha recibido un valor no adecuado para un molino de ese atributo.
    }

    public MaquinaMecanica(String marca, String modelo) {
        this(marca, modelo, DEFAULT_FUERZA_MOTRIZ);
    }

    private boolean contiene(Fuerza[] arrayDeFuerzas, Fuerza fuerzaMotriz) {
        for (Fuerza arrayDeFuerza : arrayDeFuerzas) {
            if (fuerzaMotriz.equals(arrayDeFuerza)) {
                return true;
            }
        }
        return false;
    }
}
