/*
 * La clase Batidora representa un tipo especial de máquina eléctrica con 
 * capacidad para ser enchufada, pero no recargada, ya que funciona 
 * exclusivamente conectada a la corriente, sin disponer de ningún tipo de batería.
 */
package maquina;

import java.util.Arrays;

/**
 *
 * @author
 */
public class Batidora extends MaquinaElectrica implements Enchufable {

    public static final Integer DEFAULT_VOLTAJE_BATIDORA = 230;
    public static final Double DEFAULT_POTENCIA_BATIDORA = 700.0;
    private final Integer[] listaVoltajes = {110, 120, 220, 230};
    private final Double[] listaPotencias = {500.0, 600.0, 700.0, 800.0, 1000.0, 1200.0, 1500.0};

    /**
     * Array que en cada fila establece los países que comparten un determinado
     * voltaje. Cada fila por tanto, corresponderá a un voltaje estándar
     * diferente. Este listado se usa internamente por la clase, por lo que no
     * es necesario que sea público.
     *
     */
    private final static String[][] LISTA_PAISES_COMPATIBLES = {
        {"Japón", "Corea"}, //para 110 v.
        {"USA"}, //para 120 v.
        {"China"},//para 220 v.
        {"España", "Alemania", "Francia", "Bélgica"}//para 230 v.
    };

    /**
     * Implementar un Constructor que reciba cuatro parámetros.
     *
     * @param marca
     * @param modelo
     * @param voltaje
     * @param potenciaElectrica
     */
    public Batidora(String marca, String modelo, int voltaje, Double potenciaElectrica) {
        super(marca, modelo, voltaje, potenciaElectrica);
        if (Arrays.asList(listaVoltajes).indexOf(voltaje) == -1) {                
            throw new IllegalArgumentException("Error en voltaje: "
                    + voltaje + "(Valores válidos: Japón/Corea: 110v.; USA: 120v.; China: 220v.; España/Alemania/Francia/Bélgica: 230v.).");
        }
        if (Arrays.asList(listaPotencias).indexOf(potenciaElectrica) == -1) {
            System.out.println("*** Error en potencia ***");                    
            throw new IllegalArgumentException("Error en potencia eléctrica: "
                    + potenciaElectrica + ". (Valores válidos: 500/600/700/800/1000/1200/1500 w.).");
        } 
    }

    @Override
    public int getVoltaje() {
        return this.voltaje; 
    }

    @Override
    public String[] getPaisesCompatibles() {
        String[] ret;
        ret = LISTA_PAISES_COMPATIBLES[Arrays.asList(listaVoltajes).indexOf(this.voltaje)];        
        return ret;
    }

    @Override
    public String toString() {
        String ret = super.toString().replace("}", "");
        StringBuilder sb = new StringBuilder();
        sb.append(ret);
        sb.append("Batidora{listaVoltaje=").append(listaVoltajes);
        sb.append(", valoresValidos=").append(listaPotencias);
        sb.append('}');
        return sb.toString();
    }

    
}
