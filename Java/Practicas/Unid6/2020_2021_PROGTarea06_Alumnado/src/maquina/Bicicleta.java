package maquina;

public final class Bicicleta extends MaquinaMecanica {

    double radioRueda;
    Integer totalKilometros;
    static double DEFAULT_RADIO_RUEDA = 33.0;
    public static final double MIN_RADIO_RUEDA = 17.75;
    public static final double MAX_RADIO_RUEDA = 36.85;
    static final int MAX_DESPLAZAMIENTO = 200;

    public Bicicleta(String marca, String modelo) {
        super(marca, modelo);
        this.fuerzaMotriz = Fuerza.ANIMAL;
        this.radioRueda = DEFAULT_RADIO_RUEDA;
        this.totalKilometros = 0;
    }

    public Bicicleta(String marca, String modelo, double radioRueda) {
        this(marca, modelo);

        if (radioRueda > MIN_RADIO_RUEDA && radioRueda < MAX_RADIO_RUEDA) {
            this.radioRueda = radioRueda;
            cantidadDeMaquinasFabricadas++;
        } else {
            Maquina.numeroSerie -= 1;
            StringBuilder mesg = new StringBuilder("Error en valor del radio: ");
            mesg.append(radioRueda);
            mesg.append(" cm. Debe estar comprendido entre ");
            mesg.append(MIN_RADIO_RUEDA);
            mesg.append(" cm. y ");
            mesg.append(MAX_RADIO_RUEDA);
            mesg.append(" cm.");
            throw new IllegalArgumentException(mesg.toString());
        }
    }

    @Override
    public String toString() {
        String ret = "; Fuerza Motriz: " + fuerzaMotriz + "; Radio: "
                + radioRueda + "; Kilómetros: " + totalKilometros + " }";
        StringBuilder msg = new StringBuilder(super.toString());
        msg.insert(msg.indexOf("}"), ret);
        return msg.toString();
    }

    /*Implementar el método desplazar, que recibirá como parámetro los 
    kilómetros a recorrer con la bicicleta, que no podrá ser negativo ni mayor 
    que el desplazamiento máximo establecido por la correspondiente constante
    de clase. Si no cumpliera esas condiciones, se lanzará una
    IllegalArgumentException  con el mensaje Cantidad de kilómetros negativa o 
    excesiva (Máx: XX km): YY km.*/
    public void desplazar(Integer km) {
        if (km >= 0 && km <= MAX_DESPLAZAMIENTO) {
            this.totalKilometros = km;
        } else {
            StringBuilder bici = new StringBuilder("Cantidad de kilómetros negativa o excesiva (Máx: ");
            bici.append(MAX_DESPLAZAMIENTO);
            bici.append(" km): ");
            bici.append(km);
            bici.append(" km. ");
            throw new IllegalArgumentException(bici.toString());
        }
    }

    public int getTotalKilometrosRecorridos() {
        return totalKilometros;
    }
    
    public int getTotalKilometrosSinRepostar() {
        return totalKilometros;
    }
}
