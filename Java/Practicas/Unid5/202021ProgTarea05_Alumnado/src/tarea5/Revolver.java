package tarea5;

import java.time.Year;
//import libtarea5.Audio;

// ------------------------------------------------------------
//                   Clase Revolver
// ------------------------------------------------------------
/**
 * Clase que representa un <strong>revolver</strong>.
 *
 * @author Nombre y apellidos del alumno
 */
public class Revolver {

    static final char CASQUILLO = 'x';
    static final char BALA = 'X';
    static final char HUECO = '_';
    static final int MAXIMA_CAPACIDAD = 10;
    static final int MINIMA_CAPACIDAD = 5;
    static final int DEFAULT_CAPACIDAD = 6;
    static final int MAXIMO_NUM_SERIE = 99;

    static Revolver crearRevolverCargado() {
        Revolver r = Revolver.crearRevolverCargado(DEFAULT_CAPACIDAD);
        return r;

    }

    static Revolver crearRevolverCargado(int capacidad) {
        Revolver r = new Revolver(capacidad);
        r.cargar();
        return r;
    }

    private final String numSerie;
    private final char[] tambor;
    private int posicionPercutor;
    private int numDisparos;

    static int numTotalBalasDisparadas = 0;
    static int numRevolveresDescargados = 0;
    static int siguienteNumeroSerie = 0;

    private static String crearNumeroSerie() {
        String anno;
        String serie;
        StringBuilder valorRetorno = new StringBuilder();

        // Calculo el valor del año y lo convierto a String
        anno = String.valueOf(Year.now().getValue());

        if (Revolver.siguienteNumeroSerie < Revolver.MAXIMO_NUM_SERIE + 1) {
            serie = String.format("%02d", Revolver.siguienteNumeroSerie);
            Revolver.siguienteNumeroSerie++;
        } else {
            throw new IllegalStateException("Se ha alcanzado la cantidad máxima de registros que se pueden crear este año: " + Revolver.siguienteNumeroSerie);
        }

        valorRetorno.append(anno);
        valorRetorno.append("-");
        valorRetorno.append(serie);

        return valorRetorno.toString();
    }

    static int getNumTotalDisparos() {
        return Revolver.numTotalBalasDisparadas;
    }

    static int getNumRevolveresDescargados() {
        return Revolver.numRevolveresDescargados;
    }

    public String getNumSerie() {
        return this.numSerie;
    }

    public int getCapacidad() {
        return this.tambor.length;
    }

    public int getNumBalas() {
        int ret = 0;
        for (int i = 0; i < this.getCapacidad(); i++) {
            if (tambor[i] == BALA) {
                ret++;
            }
        }
        return ret;
    }

    public int getNumDisparos() {
        return this.numDisparos;
    }

    public boolean isDescargado() {
        for (int i = 0; i < this.getCapacidad(); i++) {
            if (tambor[i] == BALA) {
                return false;
            }
        }
        return true;
    }

    int cargar(int numBalas) {        
        // Contador de balasACargar efectivamente cargadas
        int valorRetorno = 0;
        int balasACargar = numBalas;

        // Comprueba si el revólver está completamente descargado para quitarlo del contador de descargados
       if (this.isDescargado() && (Revolver.numRevolveresDescargados > 0)) {
            Revolver.numRevolveresDescargados--;
        }
        // Recorro el tambor e inserto una bala donde hay un hueco o un casquillo
        for (int i = 0; i < this.getCapacidad(); i++) {
            if ((tambor[i] == HUECO || tambor[i] == CASQUILLO) && balasACargar > 0) {
                tambor[i] = BALA;
                balasACargar--;
                valorRetorno++;
            }
        }
        // Devuelvo el número de balas insertadas
        return valorRetorno;
    }

    public int cargar() {
        int valorRetorno;
        valorRetorno = this.cargar(DEFAULT_CAPACIDAD);
        return valorRetorno;
    }

    public boolean disparar() {
        int pos = this.posicionPercutor;
        Boolean disparoEfectivo = false;
        if (this.tambor[pos] == 'X') {
            this.tambor[pos] = 'x';
            Revolver.numTotalBalasDisparadas++;
            this.numDisparos++;
            disparoEfectivo = true;
        } else {
            disparoEfectivo = false;
        }
        this.avanzaTambor();
        return disparoEfectivo;
    }

    @Override
    public String toString() {
        StringBuilder estado = new StringBuilder();
        estado.append('{');
        for (int i = 0; i < this.getCapacidad(); i++) {
            if(i == this.posicionPercutor){
                estado.append('[');
                estado.append(tambor[i]);
                estado.append(']');                
            } else {
                estado.append(' ');
                estado.append(tambor[i]);
                estado.append(' ');                
            }
        }
        estado.append('}');
        
        return estado.toString();
    }

    public int descargar() {
        int valorRetorno = 0;
        // Recorro el tambor y saco solo las balas
        for (int i = 0; i < tambor.length; i++) {
            if (tambor[i] == BALA) {
                tambor[i] = HUECO;
                valorRetorno++;
            }
        }
        // Como quedará descargdo, aumento el número de revólveres descargados
        Revolver.numRevolveresDescargados++;
        return valorRetorno;
    }

    public Revolver() {
        this(DEFAULT_CAPACIDAD);
    }

    public Revolver(int capacidad) {

        if (capacidad < Revolver.MINIMA_CAPACIDAD || capacidad > Revolver.MAXIMA_CAPACIDAD) {
            throw new IllegalArgumentException("Parámetros de creación de revólver inválidos. Capacidad del tambor: " + capacidad);
        } else {
            this.tambor = new char[capacidad];
            // Iniciamos los huecos del tambor con guiones bajos
            for (int i = 0; i < capacidad; i++) {
                tambor[i] = '_';                
            }
            this.numSerie = Revolver.crearNumeroSerie();
            this.numDisparos = 0;
            this.posicionPercutor = 0;
            Revolver.numRevolveresDescargados++;
        }
    }

    public Revolver(Revolver revolver) {
        this(revolver.getCapacidad());
    }

    private void avanzaTambor() {
        posicionPercutor = (posicionPercutor + 1) % this.getCapacidad();
    }

}
