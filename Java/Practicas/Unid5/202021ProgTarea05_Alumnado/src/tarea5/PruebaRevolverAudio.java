package tarea5;

/**
 * Programa de prueba de la clase Revolver usando efectos de sonido.
 * @author Nombre y apellidos del alumno
 */
public class PruebaRevolverAudio {

    /**
     * Programa de prueba de la clase Revolver usando efectos de sonido.
     *
     * @param args the command line arguments. Si args[0] es "1", se activará el
     * audio en la clase Revolver. En cualquier otro caso el audio estará
     * desactivado.
     */
    public static void main(String[] args) {
        System.out.print(Utilidades.cabecera("PRUEBAS CON OBJETOS DE LA CLASE REVOLVER CON AUDIO"));

        // Lectura de argumentos desde la línea de órdenes (command line)


        // Creamos un nuevo revolver con capacidad cinco balas

        
        // Realizamos dos disparos (con el tambor vacío)

        
        // Cargamos el revolver con cuatro balas

        
        // Realizamos cuatro disparos 

        
        // Cargamos completamente
        
  
        // Descargamos

        
        // Cargamos completamente



    }

}
