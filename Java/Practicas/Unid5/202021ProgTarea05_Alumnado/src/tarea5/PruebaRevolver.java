/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Programa de prueba de la clase Revolver.
 * Ejecutar y listo.
 * @author profe
 */
public class PruebaRevolver {

    /**
     * Método principal de prueba.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Revolver revolver = null;
        LocalDate fechaActual= LocalDate.now();
        DateTimeFormatter formatoFecha= DateTimeFormatter.ofPattern ("dd/MM/YYYY");
        fechaActual.format(formatoFecha);

        // Lectura de argumentos desde la línea de órdenes (command line)
        /*if (args.length>0) {
            if (args[0].equals ("1")) {
                Revolver.setAudioOn();
            }
        }*/
        System.out.print(Utilidades.cabecera("PRUEBAS CON OBJETOS DE LA CLASE REVOLVER"));
        System.out.printf ("Fecha de realización de la prueba: %s.\n", 
                fechaActual.format(formatoFecha));
        Utilidades.consultaAtributosPublicos();
        Utilidades.consultaInformacionClase();

        System.out.print(Utilidades.cabecera("PRUEBAS DEL CONSTRUCTOR CON PARÁMETROS"));
        for (int capacidad = Revolver.MINIMA_CAPACIDAD - 1; capacidad <= Revolver.MAXIMA_CAPACIDAD + 1; capacidad++) {
            try {
                System.out.printf("Intentando instanciar objeto revolver de %d balas. ", capacidad);
                revolver = new Revolver(capacidad);
                System.out.printf("Revolver creado.\n");
                System.out.printf("%s - Núm. serie: %s\n", revolver, revolver.getNumSerie());
            } catch (IllegalArgumentException | IllegalStateException ex) {
                System.out.printf("Error. %s.\n", ex.getMessage());
            }
        }

        System.out.print(Utilidades.cabecera("PRUEBA DEL CONSTRUCTOR SIN PARÁMETROS"));
        try {
            revolver = new Revolver();
            System.out.printf("Objeto revolver creado.\n");
            System.out.printf("%s - Núm. serie: %s\n", revolver, revolver.getNumSerie());
            Utilidades.consultaInformacionObjeto(revolver);
        } catch (IllegalStateException ex) {
            System.out.printf("Error: %s.\n", ex.getMessage());
        }

        // Realizamos 2 disparos (con el tambor vacío)
        Utilidades.dispararRevolver(revolver, 2);
        Utilidades.consultaInformacionObjeto(revolver);

        // Cargamos con 3 balas
        Utilidades.cargarRevolver(revolver, 3);
        Utilidades.consultaInformacionObjeto(revolver);

        // Realizamos 8 disparos
        Utilidades.dispararRevolver(revolver, 8);
        Utilidades.consultaInformacionObjeto(revolver);

        // Cargamos con 1 bala
        Utilidades.cargarRevolver(revolver, 1);
        Utilidades.consultaInformacionObjeto(revolver);

        // Cargamos con 3 balas
        Utilidades.cargarRevolver(revolver, 3);
        Utilidades.consultaInformacionObjeto(revolver);

        // Realizamos 3 disparos
        Utilidades.dispararRevolver(revolver, 3);
        Utilidades.consultaInformacionObjeto(revolver);

        // Cargamos completamente
        Utilidades.cargarRevolverCompletamente(revolver);
        Utilidades.consultaInformacionObjeto(revolver);

        // Vacíamos el revolver completamente
        Utilidades.descargarRevolver(revolver);
        Utilidades.consultaInformacionObjeto(revolver);

        // Cargamos con 2 balas
        Utilidades.cargarRevolver(revolver, 2);
        Utilidades.consultaInformacionObjeto(revolver);
        System.out.printf("Cantidad global de revólveres descargados: %d.\n",
                Revolver.getNumRevolveresDescargados());

        // Realizamos 5 disparos
        Utilidades.dispararRevolver(revolver, 5);
        Utilidades.consultaInformacionObjeto(revolver);
        System.out.printf("Cantidad global de revólveres descargados: %d.\n",
                Revolver.getNumRevolveresDescargados());

        // Vacíamos el revolver completamente
        Utilidades.descargarRevolver(revolver);
        Utilidades.consultaInformacionObjeto(revolver);
        Utilidades.consultaInformacionClase();

        System.out.print(Utilidades.cabecera("PRUEBA DEL CONSTRUCTOR COPIA "));
        try {
            Revolver r2 = new Revolver(revolver);
            System.out.printf("Objeto copia de revolver creado.\n");
            Utilidades.consultaInformacionObjeto(r2);
            System.out.printf("Cantidad global de revólveres descargados: %d.\n",
                    Revolver.getNumRevolveresDescargados());
            System.out.printf("Cargamos completamente el revolver copiado.\n");
            r2.cargar();
            System.out.printf("Cantidad global de revólveres descargados: %d.\n",
                    Revolver.getNumRevolveresDescargados());
            System.out.printf("%s\n", r2.toString());
            System.out.printf("Disparamos dos veces el revolver copiado.\n");
            for (int i = 1; i <= 2; i++) {
                r2.disparar();
            }
            System.out.printf("%s\n", r2.toString());
        } catch (IllegalStateException ex) {
            System.out.printf("Error: %s.\n", ex.getMessage());
        }

        // Obtenemos los atributos de clase
        Utilidades.consultaInformacionClase();

        System.out.print(Utilidades.cabecera("PRUEBAS DE LOS MÉTODOS \"FACTORY\""));
        for (int capacidad = Revolver.MINIMA_CAPACIDAD - 1; capacidad <= Revolver.MAXIMA_CAPACIDAD + 1; capacidad++) {
            try {
                System.out.printf("Intentando crear objeto revolver de %d balas cargado. ", capacidad);
                revolver = Revolver.crearRevolverCargado(capacidad);
                System.out.printf("Revolver cargado creado.\n");
                System.out.printf("%s - Núm. serie: %s\n", revolver, revolver.getNumSerie());
            } catch (IllegalArgumentException | IllegalStateException ex) {
                System.out.printf("Error: %s.\n", ex.getMessage());
            }
        }
        try {
            revolver = Revolver.crearRevolverCargado();
            System.out.printf("Revolver cargado con capacidad por omisión creado.\n");
            System.out.printf("%s - Núm. serie: %s\n", revolver, revolver.getNumSerie());
        } catch (IllegalStateException ex) {
            System.out.printf("Error: %s.\n", ex.getMessage());
        }

        System.out.print(Utilidades.cabecera("PRUEBAS PARA HACER SALTAR EXCEPCIÓN DEL NÚMERO DE SERIE"));
        boolean maximoSuperado = false;
        int revolveresCreados = 0;
        while (!maximoSuperado) {    // Con el constructor
            try {
                Revolver r;
                r = new Revolver();
                revolveresCreados++;
            } catch (IllegalArgumentException | IllegalStateException ex) {
                System.out.printf("Error en el constructor: \n%s\n", ex.getMessage());
                maximoSuperado = true;
            }
        }
        try {  // Con el método "fábrica"
            Revolver r;
            r = Revolver.crearRevolverCargado();
            revolveresCreados++;
        } catch (IllegalArgumentException | IllegalStateException ex) {
            System.out.printf("Error el método estático crearRevolverCargado: \n%s\n", ex.getMessage());
        }

        Utilidades.consultaInformacionClase();

        System.out.println();
    }

}
