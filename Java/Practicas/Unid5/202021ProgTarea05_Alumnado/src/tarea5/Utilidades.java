package tarea5;

/**
 * Utilidades para el programa de prueba de la clase <code>Revolver</code>.
 *
 * @author profe
 */
public class Utilidades {

    /**
     * Muestra por pantalla el contenido de los atributos públicos de la clase
     * <code>Revolver</code>
     */
    public static void consultaAtributosPublicos() {
        System.out.print(Utilidades.cabecera("CONSULTA DE LOS ATRIBUTOS PÚBLICOS"));
        System.out.printf("Capacidad máxima del tambor: %d.\n", Revolver.MAXIMA_CAPACIDAD);
        System.out.printf("Capacidad mínima del tambor: %d.\n", Revolver.MINIMA_CAPACIDAD);
        System.out.printf("Capacidad por omisión del tambor: %d.\n", Revolver.DEFAULT_CAPACIDAD);
        System.out.printf("Máximo número de serie admitido por año: %d.\n", Revolver.MAXIMO_NUM_SERIE);
    }

    /**
     * Muestra por pantalla el contenido actual de los atributos estáticos de la
     * clase <code>Revolver</code>
     */
    public static void consultaInformacionClase() {
        System.out.print(cabecera("CONSULTA DE INFORMACIÓN DE CLASE REVOLVER"));
        System.out.printf("Cantidad global de disparos realizados por todos los revólveres: %d.\n",
                Revolver.getNumTotalDisparos());
        System.out.printf("Cantidad global de revólveres descargados: %d.\n",
                Revolver.getNumRevolveresDescargados());
    }

    /**
     * Muestra por pantalla el estado de un objeto instancia de la clase
     * <code>Revolver</code>
     *
     * @param r objeto revólver cuyo estado se desea mostrar por pantalla
     */
    public static void consultaInformacionObjeto(Revolver r) {
        System.out.print(cabecera("CONSULTA DE INFORMACIÓN DE OBJETO REVOLVER"));
        System.out.printf("Número de serie: %s.\n", r.getNumSerie());
        System.out.printf("Capacidad del tambor: %d.\n", r.getCapacidad());
        System.out.printf("Número de balas cargadas actuales: %d.\n", r.getNumBalas());
        System.out.printf("Número total de disparos realizados: %d.\n", r.getNumDisparos());
        System.out.printf("Está descargado: %s.\n", r.isDescargado() ? "sí" : "no");
        System.out.printf("Estado actual del tambor: %s.\n", r.toString());
    }

    /**
     * Carga un revólver con un número de balas usando el método
     * <code>cargar</code> de la clase <code>Revolver</code>. Muestra por
     * pantalla el estado antes y después de cargar
     *
     * @param revolver objeto revólver que se va a cargar
     * @param numBalas número de balas con las que se va a cargar
     */
    public static void cargarRevolver(Revolver revolver, int numBalas) {
        int balas;
        String texto = String.format("CARGAMOS EL ARMA CON %d BALA%s",
                numBalas, numBalas == 1 ? "" : "S");
        System.out.print(cabecera(texto));
        System.out.printf("Estado del revolver antes de cargar: \n%s.\n", revolver);
        System.out.printf("Cargando...\n");
        balas = revolver.cargar(numBalas);
        System.out.printf("Cargado. Balas cargadas efectivamente: %d.\n", balas);
        System.out.printf("Estado del revolver después de cargar: \n%s.\n", revolver);
    }

    /**
     * Carga un revólver completamente usando el método cargar de la clase
     * <code>Revolver</code>. Muestra por pantalla el estado antes y después de
     * cargar
     *
     * @param revolver objeto revólver que se va a cargar
     */
    public static void cargarRevolverCompletamente(Revolver revolver) {
        int balas;
        String texto = String.format("CARGAMOS EL ARMA COMPLETAMENTE");
        System.out.print(cabecera(texto));
        System.out.printf("Estado del revolver antes de cargar: \n%s.\n", revolver);
        System.out.printf("Cargando...\n");
        balas = revolver.cargar();
        System.out.printf("Cargado. Balas cargadas efectivamente: %d.\n", balas);
        System.out.printf("Estado del revolver después de cargar: \n%s.\n", revolver);
    }

    /**
     * Dispara un revólver una cantidad de veces indicada por parámetro usando
     * el método <code>disparar</code> de la clase <code>Revolver</code>.
     * Muestra por pantalla el estado antes y después de efectuar cada disparo.
     *
     * @param revolver objeto revólver que se va a disparar
     * @param numDisparos cantidad de disparos que se se desan realizar
     */
    public static void dispararRevolver(Revolver revolver, int numDisparos) {
        boolean disparoEfectivo;
        String texto = String.format("DISPARAMOS EL ARMA %d VE%s",
                numDisparos, numDisparos == 1 ? "Z" : "CES");
        System.out.print(cabecera(texto));
        for (int i = 1; i <= numDisparos; i++) {
            System.out.printf("Disparo %d:\n", i);
            System.out.printf("Estado del revolver antes de disparar: \n%s.\n", revolver);
            disparoEfectivo = revolver.disparar();
            System.out.printf("Disparo %s efectivo\n", disparoEfectivo ? "" : " no");
            System.out.printf("Estado del revolver después de disparar: \n%s.\n\n", revolver);
        }
    }

    /**
     * Descarga un revólver usando el método <code>descargar</code> de la clase
     * <code>Revolver</code>. Muestra por pantalla el estado antes y después de
     * descargar
     *
     * @param revolver objeto revólver que se va a descargar
     */
    public static void descargarRevolver(Revolver revolver) {
        System.out.print(cabecera("VACIAMOS EL TAMBOR"));
        System.out.printf("Estado del revolver antes de descargar: \n%s.\n", revolver);
        System.out.printf("Balas descargadas: %d.\n", revolver.descargar());
        System.out.printf("Estado del revolver después de descargar: \n%s.\n", revolver);
    }

    /**
     * Genera un título de cabecera con adornos en las líneas anterior y
     * posterior.
     *
     * @param texto título que se desea adornar
     * @return cadena con los adornos lista para mostrar por pantalla
     */
    public static String cabecera(String texto) {
        StringBuilder cabecera = new StringBuilder();
        StringBuilder lineas = new StringBuilder();
        // el método repeat solo está disponible a partir de Java 11
        //String lineas = "-".repeat(texto.length());
        for (int i = 0; i < texto.length(); i++) {
                lineas.append("-");
        }
        cabecera.append(lineas).append("\n").append(texto).append("\n").append(lineas).append("\n");

        return cabecera.toString();
    }

}
