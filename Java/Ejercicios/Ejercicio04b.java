package Ejercicios;


import java.util.Scanner;

public class Ejercicio04b
{


    public static void main(String[] args)
    {

        //----------------------------------------------
        //          Declaración de variables
        //----------------------------------------------

        // Constantes

        // Variables de entrada
        int nFilas;


        // Variables de salida
        String resultado = (" ");

        // Variables auxiliares
        int contador = 0;
        int cantidadActual = 0;
        int i = 1;

        // Clase Scanner para petición de datos de entrada
        Scanner teclado = new Scanner (System.in);


        //----------------------------------------------
        //                Entrada de datos
        //----------------------------------------------
        System.out.println("ESCALERA INCREMENTAL ");
        System.out.println("--------------------------------------\n");

        do
        {
            System.out.print("Introduzca número de filas (1-10):  ");
            nFilas = teclado.nextInt();
            contador++;

        }
        while (nFilas < 0 || nFilas > 10);

        //----------------------------------------------
        //                 Procesamiento
        //----------------------------------------------

        resultado = ("");

        for ( i = 1; i <= nFilas; i++)
        {
            //System.out.println("");
            resultado =  resultado + "\n" + i + ": ";

            for (int j = 1; j <= i; j++)
            {
                cantidadActual = cantidadActual + 1;
                resultado = resultado + cantidadActual + " ";
            }
        }

        //----------------------------------------------
        //              Salida de resultados
        //----------------------------------------------

        System.out.println ("RESULTADO");
        System.out.println ("-----------");

        System.out.print (resultado);
        System.out.println ("\n\nFin del programa. Bye!");

    }  //Fin método main


}  //Fin Ejercicio04