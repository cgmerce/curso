import java.util.Scanner;

public class Ejercicio02b
{

    public static void main(String[] args)
    {

        //----------------------------------------------
        //          Declaración de variables
        //----------------------------------------------

        // Constantes

        // Variables de entrada
        int mes = 0;

        // Variables de salida
        int numeroDias = 0;

        // Variables auxiliares

        // Clase Scanner para petición de datos de entrada
        Scanner teclado = new Scanner (System.in);

        //----------------------------------------------
        //                Entrada de datos
        //----------------------------------------------
        System.out.println("NÚMERO DE DÍAS DEL MES ");
        System.out.println("------------------------\n");
        System.out.print("Introduzca mes (1-12): ");
        mes = teclado.nextInt ();


        //----------------------------------------------
        //                 Procesamiento
        //----------------------------------------------


        if (mes > 0 && mes < 13)
            switch(mes)
            {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                numeroDias = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                numeroDias = 30;
                break;
            case 2:
                numeroDias = 28;
                break;
            default:
                numeroDias = 0;
                break;
            } //Fin switch.

        //----------------------------------------------
        //              Salida de resultados
        //----------------------------------------------

        System.out.println ("RESULTADO");
        System.out.println ("-----------");


        System.out.println ("El número de días para ese mes es:  " + numeroDias);
        System.out.println ("\n\nFin del programa. Bye!");

    }    //Fin método main

} // Fin Ejercicio02