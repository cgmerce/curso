package Ejercicios;

import java.util.Scanner;

public class Ejercicio03 {

    public static void main(String[] args) {

        //----------------------------------------------
        //          Declaración de variables 
        //----------------------------------------------
        // Constantes
        final int MIN_LARGOS = 0;
        final int MAX_LARGOS = 50;
        final int INTENTOS = 3;

        // Variables de entrada
        int contador;              //nombre de la variable contador.
        int numeroIntentos = 3;   //contador de intentos. Máximo 3 intentos.
        int numeroLargos;
        boolean entradaValida = true;

        // Variables de salida
        String listaLargos = (" ");

        // Variables auxiliares
        // Clase Scanner para petición de datos de entrada
        Scanner teclado = new Scanner(System.in);

        //----------------------------------------------
        //                Entrada de datos 
        //----------------------------------------------
        System.out.println("ENTRENAMIENTO DE NATACIÓN ");
        System.out.println("---------------------------\n");

        do {
            System.out.println("Introduzca el número de largos realizados (entre 0 y 50):  ");
            numeroLargos = teclado.nextInt();
            numeroIntentos++;
            if (numeroLargos < MIN_LARGOS || numeroLargos > MAX_LARGOS) {
                entradaValida = false;
            }

        } while (!entradaValida && numeroIntentos < INTENTOS);

        //----------------------------------------------
        //                 Procesamiento 
        //----------------------------------------------
        for (contador = 1; contador <= numeroLargos; contador++) {  //Incrementamos el contador si corresponde.
            if (contador % 2 == 0) {
                listaLargos = listaLargos + " Espalda";
            }

            //else if ((4*contador)-3)
            listaLargos = listaLargos + " Crol";
            //else
            listaLargos = listaLargos + " Braza";

        } // fin bucle con contador (for).

        //----------------------------------------------
        //              Salida de resultados 
        //----------------------------------------------
        System.out.println("DESARROLLO DE LARGOS");
        System.out.println("---------------------\n");
        System.out.println(listaLargos + "\n");
        System.out.println("Fin del programa. Bye!");

    }    //Fin método main

}  //Fin Ejercicio04
