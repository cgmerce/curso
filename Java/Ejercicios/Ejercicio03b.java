package Ejercicios;

import java.util.Scanner;

public class Ejercicio03b {

    public static void main(String[] args) {

        //----------------------------------------------
        //          Declaración de variables
        //----------------------------------------------
        // Constantes
        final int MIN_LARGOS = 0;
        final int MAX_LARGOS = 50;
        final int INTENTOS = 3;

        // Variables de entrada
        int contador;              //nombre de la variable contador.
        int numeroIntentos = 0;   //contador de intentos. Máximo 3 intentos.
        int numeroLargos;
        boolean entradaValida = true;

        // Variables de salida
        String listaLargos;

        // Variables auxiliares
        // Clase Scanner para petición de datos de entrada
        Scanner teclado = new Scanner(System.in);

        //----------------------------------------------
        //                Entrada de datos
        //----------------------------------------------
        System.out.println("ENTRENAMIENTO DE NATACIÓN ");
        System.out.println("---------------------------\n");

        do {
            System.out.print("Introduzca el número de largos realizados (entre 0 y 50):  ");
            numeroLargos = teclado.nextInt();
            numeroIntentos++;
            if (numeroLargos < MIN_LARGOS || numeroLargos > MAX_LARGOS) {
                entradaValida = false;
            }
            System.out.println("\n");

        } while (!entradaValida && numeroIntentos < INTENTOS);

        //----------------------------------------------
        //                 Procesamiento
        //----------------------------------------------
        listaLargos = ("{ ");

        
        // En caso de que no sea par el tipo indica si es Crol o Braza
        int tipo = 1;

        for (contador = 0; contador < numeroLargos; contador++) {  //Incrementamos el contador si corresponde.            

            // Cuando el contador es par, el estilo es siempre Espalda
            if (contador % 2 == 1) {
                listaLargos += "Espalda";               
            } else {
                // Si el contador es impar, se alternan Crol y Braza
                // Tipo nos indica que estilo toca
                if (tipo == 1) {
                    listaLargos += "Crol";
                } else {
                    listaLargos += "Braza";
                }
                
                // Cambiamos el tipo del estilo impar para el prócimo ciclo
                tipo *= -1;
            }
            
            // Mientras el contador sea menor que el número de largos, insertamos una coma separadora
            if (contador < numeroLargos - 1) {
                listaLargos += ", ";
            }

        } // fin bucle con contador (for).

        // Para finalizar, cerramos las llaves
        listaLargos += (" }");

        //----------------------------------------------
        //              Salida de resultados
        //----------------------------------------------
        // Crol Espalda Braza Espalda Crol Espalda Braza Espalda Crol Espalda Braza
        System.out.println("\nDESARROLLO DE LARGOS");
        System.out.println("---------------------\n");
        System.out.println(listaLargos + "\n");
        System.out.println("Fin del programa. Bye!");

    }    //Fin método main

}  //Fin Ejercicio04
