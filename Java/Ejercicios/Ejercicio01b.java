package Ejercicios;

import java.util.Scanner;

public class Ejercicio01b
{
    //el método main comienza la ejecución de la aplicación
    public static void main(String[] args)
    {

        //-------------------------------------------------------------------------
        //                    Declaración de variables
        //-------------------------------------------------------------------------

        //Constantes

        //Variables de entrada
        int dia, mes;
        boolean fechaValida = false;

        //Variables de salida
        String estacion = "";

        //Clase Scanner para petición de datos de entrada
        Scanner sc = new Scanner(System.in);

        //-------------------------------------------------------------------------
        //                     Entrada de datos
        //-------------------------------------------------------------------------
        System.out.println("ESTACIONES Y CUMPLEAÑOS: ");
        System.out.println("-------------------------\n ");
        System.out.println("Introduzca dia de nacimiento (1-31): "); //leer el primer número
        dia = sc.nextInt ();
        System.out.println("Introduzca mes de nacimiento (1-12): "); //leer el segundo número
        mes = sc.nextInt ();

        if (dia < 1 || dia > 31)
            fechaValida = false;
        else if (mes < 1 || mes > 12)
            fechaValida = false;
        else if (mes == 2 && dia > 28)
            fechaValida = false;
        else if (mes == 4 && dia > 30 || mes == 6 && dia > 30 || mes == 8 && dia > 30 || mes == 11 && dia > 30)
            fechaValida = false;
        else
            fechaValida = true;

        System.out.print("La fecha es: ");
        if (fechaValida)
            System.out.println("Válida");
        else
            System.out.println("Errónea");

        //------------------------------------------------------------------------
        //                       Procesamiento
        //------------------------------------------------------------------------

        // Primavera -> (dia > 20 && mes == 3) || (mes > 3 && mes < 6) || (dia < 21 && mes == 6)
        // Verano    -> (dia > 20 && mes == 6) || (mes > 6 && mes < 9) || (dia < 23 && mes == 9)
        // Otoño     -> (dia > 22 && mes == 9) || (mes > 9 && mes < 12) || (dia < 21 && mes == 12)
        // Invierno  -> (dia > 20 && mes == 12) || (mes < 3) || (dia < 21 && mes = 3)

        //if (fechaValida)    // Si la fecha era válida, calculamos en qué estación estamos
        //{            
            
        //}

        //-------------------------------------------------------------------------
        //                      Salida de resultados
        //-------------------------------------------------------------------------

        System.out.println("RESULTADO");
        System.out.println("----------------------- ");

        if (fechaValida)
        {
            if ((dia > 20 && mes == 3) || (mes > 3 && mes < 6) || (dia < 21 && mes == 6))
                estacion = "PRIMAVERA";
            else if ((dia > 20 && mes == 6) || (mes > 6 && mes < 9) || (dia < 23 && mes == 9))
                estacion = "VERANO";
            else if ((dia > 22 && mes == 9) || (mes > 9 && mes < 12) || (dia < 21 && mes == 12))
                estacion = "OTOÑO";
            else // Si no se da ninguna de las premisas anteriores no hay que comprobar más. Tiene que ser invierno...
                estacion = "INVIERNO";
            System.out.println("Usted nació en " + estacion);
        }
        else
        {
            System.out.println("La fecha de nacimiento introducida no es válida.");
        }

        System.out.println("Fin de impresión.");

    }//Fin método main

} //Fin Ejercicio01