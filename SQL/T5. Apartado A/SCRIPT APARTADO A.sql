/* HACER ESTO LO PRIMERO */
 SET SQL_SAFE_UPDATES = 0;
/* Para que se puedan hacer UPDATES sin cláusula WHERE en PK */

SELECT * FROM Tarea5_BD2021.PEDIDO;

/*APARTADO A

/*1.- Insertar los siguientes datos en la tabla PEDIDO teniendo en cuenta que debes insertar 
sólo los valores necesarios en los campos correspondientes.*/
INSERT INTO PEDIDO VALUES ('0012','2020-11-16',20,'03232323P','04444444T',null,'18:00:00','18:03:00',null);

INSERT INTO PEDIDO VALUES ('0013','2021-01-01',67,'55555555J',null,null,'14:00:00',null,null);

INSERT INTO PEDIDO VALUES ('0014','2021-01-15',13,'99999999X','55555555J','04477744T','21:20:00','21:30:00','21:51:00');


/*2.- Incrementar un 10% el importe de todos los pedidos 
que se han realizado en el mes de Noviembre de 2020. (Debes hacerlo con una única sentencia)*/
UPDATE PEDIDO SET Importe = (Importe * 1.10) 
WHERE Fecha BETWEEN '2020-11-01' AND '2020-11-30';


/*3.- Eliminar los pedidos cuyo tiempo de reparto ha sido de 25 minutos o más (desde que éstos estaban preparados)
 y además estaban asignados al repartidor de nombre Alejandro Pardo López.
 (Debes hacerlo con una única sentencia)*/
DELETE FROM PEDIDO
WHERE TIMESTAMPDIFF(MINUTE, Hora_pre, Hora_rep )>=25 IN (SELECT E.Nombre FROM EMPLEADO E 
WHERE E.Nombre like 'Alejandro Pardo López');
  
 /*4.- Aumentar el incentivo de los repartidores en 50 euros, a todos aquellos 
 que han repartido dos o más pedidos. (Debes hacerlo con una única sentencia)*/
 UPDATE REPARTIDOR SET Incentivo = Incentivo + 50
 WHERE DNI_R IN (SELECT DNI_R, COUNT(Hora_rep ) >=2 FROM PEDIDO);
 
 /* Arreglado por Pablo */
 select a.DNI, a.Incentivo, b.DNI_R 
 from REPARTIDOR as a
  JOIN (select DNI_R from (SELECT DNI_R, COUNT(DNI_R) as contador FROM PEDIDO group by DNI_R) as t where contador >= 2) as b
  ON a.DNI = b.DNI_R;
  
  UPDATE REPARTIDOR as a JOIN (select DNI_R from (SELECT DNI_R, COUNT(DNI_R) as contador
		FROM PEDIDO group by DNI_R) as t where contador >= 2) as b
		ON a.DNI = b.DNI_R
	SET Incentivo = Incentivo + 50; 
 
 UPDATE REPARTIDOR
  SET Incentivo = Incentivo + 50
  WHERE DNI IN (select DNI_R from (SELECT DNI_R, COUNT(DNI_R) as contador FROM PEDIDO group by DNI_R) as t where contador >= 2);

/*5.- Insertar todos los pedidos repartidos en la tabla PEDIDOS_FINALIZADOS
 incluyendo además de los campos propios de la tabla pedidos, el tiempo transcurrido
 desde que se toma nota hasta su reparto.(Debes hacerlo con una única sentencia)*/
 
 INSERT INTO PEDIDOS_FINALIZADOS
	SELECT *, DATEDIFF(Hora_tm, Hora_rep) AS Tiempo_transcurrido
	FROM PEDIDO
    WHERE Hora_tm is not null;
    
    /* Arreglado por Pablo */
    INSERT INTO PEDIDOS_FINALIZADOS
	SELECT *, DATEDIFF(Hora_tm, Hora_rep) AS Tiempo_transcurrido
	FROM PEDIDO
    WHERE Hora_rep is not null;
 
 /*6.- Insertar en la tabla RANKING_PRODUCTOS por cada producto, su código, su nombre
 y la cantidad total pedida.(Debes hacerlo con una única sentencia)*/
 INSERT INTO RANKING_PRODUCTOS
	SELECT P.Codigo, P.Nombre, C.cantidad as 'Cantidad total pedida'
	FROM PRODUCTO P, consta C
	WHERE P.Codigo = C.Codigo_Pr 
    GROUP BY P.Codigo;
    
    /* Corregido por Pablo */
    INSERT INTO RANKING_PRODUCTOS    
    select PR.Codigo, PR.Nombre, SUM(C.Cantidad) from (select * from PRODUCTO as P        
			left JOIN esta_compuesto as EC on P.Codigo = EC.Codigo_P_compuesto
			UNION
		 select * from PRODUCTO as P        
			right JOIN esta_compuesto as EC on P.Codigo = EC.Codigo_P_compuesto) as PR
	JOIN consta as C on PR.Codigo = C.Codigo_Pr
    GROUP BY PR.Codigo, PR.Nombre;
    
 /*7.- Incrementar el salario de los empleados en 10 euros si han tomado nota de algún pedido.
 Además, esos empleados también deben haber preparado algún pedido.
 (Debes hacerlo con una única sentencia)*/
 UPDATE EMPLEADO SET Salario = Salario + 10;
 
 /* Arreglado por Pablo */
 UPDATE EMPLEADO AS E SET Salario = Salario + 10 
 WHERE E.DNI IN (select DISTINCT A.DNI from
					(select E.Nombre, E.DNI, P.DNI_ETM from EMPLEADO AS E join PEDIDO AS P on E.DNI = P.DNI_ETM) as A
					INNER JOIN
					(select E.Nombre, E.DNI, P.DNI_EP from EMPLEADO AS E join PEDIDO AS P on E.DNI = P.DNI_EP) as B
					ON A.DNI = B.DNI);
  /* */
  
 /*APARTADO B