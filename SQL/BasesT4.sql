SELECT * FROM PRODUCTO;
SELECT Numero, DNI_R, Hora_rep FROM PEDIDO WHERE Hora_tm > '19:00';
SELECT * FROM EMPLEADO WHERE Salario BETWEEN 900 AND 1000;
SELECT Numero as Numero_de_pedido, Importe FROM PEDIDO WHERE Fecha BETWEEN '2020-11-01' AND '2020-11-30' AND Importe >= 15;
SELECT DNI_R, COUNT(Hora_rep) as Cant_pedidos_Entregados FROM PEDIDO GROUP BY DNI_R; 
SELECT MONTHNAME(Fecha) as Mes, COUNT(*) AS Cant_pedidos_realizados, MONTH(Fecha) FROM PEDIDO WHERE Fecha IS NOT NULL GROUP BY MONTH (Fecha);
SELECT CONCAT (DNI, " , " , Nombre) as DNI_Nombre FROM EMPLEADO WHERE Turno IN ('tarde', 'noche') ORDER BY DNI;
SELECT Nombre, Codigo, Precio FROM PRODUCTO WHERE Precio >= (SELECT AVG (Precio) FROM PRODUCTO) ORDER BY Precio DESC;
SELECT E.Nombre, E.DNI FROM EMPLEADO as E WHERE E.DNI NOT IN (SELECT DISTINCT P.DNI_EP FROM PEDIDO as P);


/*10.- Obtener el código, nombre y precio de los productos(estos dos últimos en el mismo campo)
 que están contenidos en los pedidos que ha tomado nota "Luis" o "María Luisa". 
 Ordena el listado de mayor a menor valor por fecha del pedido.*/
SELECT P.Codigo, CONCAT(P.Nombre, "  ", P.Precio) as 'Nombre y precio'
FROM PEDIDO as PD
	INNER JOIN
	 EMPLEADO as E on PD.DNI_ETM = E.DNI
	INNER JOIN
     consta as C on PD.Numero = C.Numero_P
	INNER JOIN
     PRODUCTO as P on C.Codigo_Pr = P.Codigo
     
WHERE E.Nombre like 'Luis%' OR E.Nombre like 'María Luisa%'
ORDER BY PD.Fecha DESC;

/*11.- Obtener por cada repartidor, su nombre, cantidad de pedidos entregados
 y el tiempo medio que tardan en entregar los pedidos una vez preparados. 
 Ordenar el listado por el tiempo medio que tardan en entregarlos de menor a mayor.*/

SELECT R.Nombre, COUNT(*) AS 'Cant_Ped_Entregados', TIMESTAMPDIFF(MINUTE, PD.Hora_pre, PD.Hora_rep ) AS 'Tiempo_Medio'
FROM REPARTIDOR R JOIN PEDIDO PD
WHERE R.DNI = PD.DNI_R
GROUP BY R.Nombre, PD.Hora_pre, PD.Hora_rep;

/* Corregido por Pablo */
SELECT R.Nombre, COUNT(PD.DNI_R) AS contador, avg(TIMESTAMPDIFF(MINUTE, PD.Hora_pre, PD.Hora_rep)) AS 'Tiempo_Medio'
FROM PEDIDO as PD JOIN REPARTIDOR as R
ON PD.DNI_R = R.DNI
GROUP BY R.DNI;

/*12.- Obtener un listado obteniendo el código, nombre y el precio de los productos 
cuyo precios sea el más barato o el más caro de todos.*/

SELECT Codigo, Nombre, Precio FROM PRODUCTO WHERE Precio=(SELECT MIN(Precio) FROM PRODUCTO) OR 
Precio = (SELECT MAX(Precio) FROM PRODUCTO);            

/*13.- Obtener por cada producto , el nombre, el código y el número total de pedidos
 en los que se encuentra teniendo en cuenta que el total de pedidos en los cuales 
 se encuentre sea superior o igual a dos. Ordena el listado de mayor a menor cantidad de pedidos.*/
 
 SELECT P.Nombre, P.Codigo, COUNT(PD.Numero) AS 'Num_total_ped'
 FROM PRODUCTO AS P, PEDIDO AS PD, consta AS C
 WHERE C.Codigo_Pr = P.Codigo 
 AND C.Numero_P = PD.Numero
 GROUP BY P.Nombre
 HAVING Num_total_ped>=2
 ORDER BY Num_total_ped DESC;
 
/* Corregido por Pablo */
SELECT P.Nombre, P.Codigo, COUNT(PD.Numero) AS 'Num_total_ped'
 FROM PEDIDO AS PD
	JOIN consta AS C
    ON C.Numero_P = PD.Numero
	JOIN PRODUCTO AS P
    ON P.Codigo = C.Codigo_Pr
 GROUP BY P.Codigo 
 HAVING Num_total_ped >= 2
 ORDER BY Num_total_ped DESC;
 
 /*14.- Mostrar listado de los empleados con el nombre y (el DNI y NSS en la misma columna) 
 que han tomado nota de algún pedido y contienen el producto de código 13
 y además el repartidor sea 'Laura'.*/
 
SELECT E.Nombre, CONCAT(E.DNI, ', ', E.NSS) as 'DNI_y_NSS', P.Codigo
FROM EMPLEADO as E, PEDIDO as PD, PRODUCTO as P, consta as C
WHERE E.DNI = PD.DNI_ETM
AND PD.Numero = C.Numero_P
AND C.Codigo_Pr = P.Codigo
AND P.Codigo LIKE '%13%'
AND PD.DNI_R in (SELECT PD.DNI_R FROM PEDIDO PD, REPARTIDOR R WHERE R.DNI = PD.DNI_R and R.Nombre LIKE '%Laura%');
 
/* Corregido por Pablo */
SELECT E.Nombre, CONCAT(E.DNI, ', ', E.NSS) AS 'DNI_y_NSS', PR.Codigo
 FROM PEDIDO AS P 
	JOIN EMPLEADO AS E ON P.DNI_ETM = E.DNI
    JOIN consta AS C ON C.Numero_P = P.Numero
    JOIN REPARTIDOR AS R ON R.DNI = P.DNI_R AND R.Nombre LIKE "Laura%"
    JOIN PRODUCTO AS PR ON PR.Codigo = 13
    GROUP BY E.DNI;
 
 /*15.- Obtener el nombre del producto que es menú junto con el código de los productos 
 que lo componen en aquellos pedidos del mes de septiembre de 2020.*/

SELECT P.Nombre, EC.Codigo_P_compuesto
FROM PEDIDO as PD, PRODUCTO as P, esta_compuesto as EC, consta as C
WHERE PD.Fecha IN (SELECT PD.Fecha FROM PEDIDO PD WHERE PD.Fecha BETWEEN '2020-09-01' AND '2020-09-30')
AND P.Codigo = EC.Codigo_P
AND P.Codigo = C.Codigo_Pr
AND C.Numero_P = PD.Numero;

/* Corregido por Pablo */
SELECT  P.Nombre, EC.Codigo_P_compuesto
  FROM PEDIDO as PD
	JOIN consta as C
		ON PD.Numero = C.Numero_P
	JOIN PRODUCTO as P
		ON P.Codigo = C.Codigo_Pr
	JOIN esta_compuesto as EC
		ON P.Codigo = EC.Codigo_P
  WHERE PD.Fecha BETWEEN '2020-09-01' AND '2020-09-30'
