<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
        
    <xsl:template match="/">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
            <head>
                <title>Actividad</title>
            </head>
            <body>

                <h3>1. Listado de los 10 primeros habitantes. Indicar número de orden mediante xls: </h3>
                <h5>Formato: 1.- AMO FLORES, SANDRA.</h5>                
                               
                <xsl:for-each select="/habitantes/habitante">
                    <xsl:choose>
                        <xsl:when test="position() &lt;= 10">
                            <xsl:number format="1.- "/>
                            <xsl:value-of select="apellidos"/>
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="nombre"/>
                            <xsl:text>.</xsl:text>
                            <br/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>

                <!--Sustituir por la sentencia xsl-->

                <h3>2. Listado de todos los habitantes mayores de 55 años. Indicar el número de orden mediante xls que tiene cada habitante en el archivo xml y la edad entre paréntesis: </h3>
                <h5>Formato: 58.- CARVAJAL IZQUIERDO, FRANCISCO JOSE. (59 años).</h5>
                
                <xsl:for-each select="/habitantes/habitante">
                    <xsl:choose>
                        <xsl:when test="edad &gt;= 55">
                            <xsl:number format="1.- "/>
                            <xsl:value-of select="apellidos"/>
                            <xsl:text>, </xsl:text>
                            <xsl:value-of select="nombre"/>
                            <xsl:text>. (</xsl:text>
                            <xsl:value-of select="edad"/>
                            <xsl:text>).</xsl:text>
                            <br/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
                
                <!--Sustituir por la sentencia xsl-->

                <h3>3. De los 25 primeros habitantes del censo, listar los que tienen 15 años o menos. Indicar el número de orden (con letra) mediante xls que tiene cada habitante en el archivo xml y la edad entre paréntesis: </h3>
                <h5>Formato: a) SANDRA AMO FLORES. (15 años).</h5>
                
                <xsl:for-each select="/habitantes/habitante">
                    <xsl:choose>
                        <xsl:when test="position() &lt;= 25 and edad &lt;= 15">
                            <xsl:number format="a) "/>
                            <xsl:value-of select="nombre"/>
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="apellidos"/>
                            <xsl:text>. (</xsl:text>
                            <xsl:value-of select="edad"/>
                            <xsl:text> años).</xsl:text>
                            <br/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
                
                <!--Sustituir por la sentencia xsl-->

                <h3>4. El primer y último habitante del listado. Indicar su edad y localidad: </h3>
                <h5>Resultado: Listado desde SANDRA AMO FLORES (15 años) de LOS SANTOS DE MAIMONA hasta INMACULADA ZHOU (19 años) de FUENTE DE CANTOS.</h5>
                                       
                <xsl:text> Listado desde </xsl:text>
                <xsl:value-of select="(/habitantes/habitante/nombre)[1]"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="(/habitantes/habitante/apellidos)[1]"/>
                <xsl:text> (</xsl:text>
                <xsl:value-of select="(/habitantes/habitante/edad)[1]"/>
                <xsl:text> años) de </xsl:text>
                <xsl:value-of select="(/habitantes/habitante/localidad)[1]"/>
                <xsl:text> hasta </xsl:text>
                <xsl:value-of select="(/habitantes/habitante/nombre)[last()]"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="(/habitantes/habitante/apellidos)[last()]"/>
                <xsl:text> (</xsl:text>
                <xsl:value-of select="(/habitantes/habitante/edad)[last()]"/>
                <xsl:text> años) de </xsl:text>
                <xsl:value-of select="(/habitantes/habitante/localidad)[last()]"/>
                <xsl:text>. </xsl:text>
                                
                <!--Sustituir por la sentencia xsl-->
                
                <h3>5. Total de habitantes censados y total de habitantes de AGUADULCE: </h3>
                <h5>Formato: Hay XXX habitantes censados en total, de los cuales XXX son de AGUADULCE</h5>
                
                <xsl:text>Hay </xsl:text>
                <xsl:value-of select="(count(/habitantes/habitante))"/>
                <xsl:text> habitantes censados en total, de los cuales </xsl:text>
                <xsl:value-of select="count(/habitantes/habitante[localidad='AGUADULCE'])"/>
                <xsl:text> son de AGUADULCE</xsl:text>                
                                
                <!--Sustituir por la sentencia xsl-->

                <h3>6. Lista ordenada con el nombre, apellido, edad y DNI de todos los habitantes llamados MIGUEL GOMEZ o FRANCISCO MANUEL, ordenados por edad: </h3>
                <h5>Formato: 1.- MIGUEL GOMEZ COBO (15 años). (44789099A).</h5>
                
                <ol>
                    <xsl:for-each select="/habitantes/habitante">
                        <xsl:sort select = "edad"/>
                        <xsl:choose>
                            <xsl:when test="(nombre='MIGUEL' and contains(apellidos,'GOMEZ')) or (nombre='FRANCISCO MANUEL')">
                                <li>
                                    <xsl:text>-</xsl:text>
                                    <xsl:value-of select="nombre"/>
                                    <xsl:text> </xsl:text>
                                    <xsl:value-of select="apellidos"/>
                                    <xsl:text>. (</xsl:text>
                                    <xsl:value-of select="edad"/>
                                    <xsl:text> años).</xsl:text>
                                    <xsl:text> (</xsl:text>
                                    <xsl:value-of select="@dni"/>
                                    <xsl:text> ).</xsl:text>
                                </li>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:for-each>
                </ol>
                
                <!--Sustituir por la sentencia xsl-->

                <h3>7. Número de habitantes menores de 55 años que viven en ROQUETAS DE MAR y el porcentaje que supone con respecto al total de habitantes en la misma localidad: </h3>
                <h5>Formato: Hay XX habitantes menores de 55 años, que suponen un XX.XX% del total de habitantes en Roquetas de Mar.</h5>
                                
                <xsl:variable
                    name="totalHabitantes"
                    select="count(/habitantes/habitante[localidad='ROQUETAS DE MAR'])">
                </xsl:variable>                 
                <xsl:variable
                    name="totalHabitantesEdad"
                    select="count(/habitantes/habitante[edad &lt; 55 and localidad='ROQUETAS DE MAR'])">
                </xsl:variable> 
                <xsl:variable
                    name="porcentaje"
                    select="($totalHabitantesEdad * 100) div $totalHabitantes">
                </xsl:variable> 
                <xsl:text>Hay </xsl:text>
                <xsl:value-of select="$totalHabitantesEdad"/>
                <xsl:text> habitantes menores de 55 años, que suponen un </xsl:text>
                <xsl:value-of select="format-number($porcentaje, '#.00')"/>
                <xsl:text>% del total de habitantes en Roquetas de Mar.</xsl:text>
                    
                <!--Sustituir por la sentencia xsl-->

                <h3>8. Lista ordenada con el apellido, nombre completo y DNI de todos los habitantes que su nombre comience por Manuel y su DNI tenga la letra G, ordenados alfabéticamente por apellido: </h3>
                <h5>Formato: 1. MURILLO GARCIA, MANUEL (08866481G).</h5>
                
                <ol>
                    <xsl:for-each select="/habitantes/habitante">
                        <xsl:sort select="apellidos"/>                            
                        <xsl:if test='(starts-with(nombre,"MANUEL") and contains(@dni,"G"))'>
                            <li>
                                <xsl:value-of select="apellidos"/>
                                <xsl:text>, </xsl:text>
                                <xsl:value-of select="nombre"/>
                                <xsl:text> (</xsl:text>
                                <xsl:value-of select="@dni"/>
                                <xsl:text>).</xsl:text>
                            </li>
                        </xsl:if>
                    </xsl:for-each>
                </ol>  
                
                <!--Sustituir por la sentencia xsl-->

                <h3>9. Todas las localidades separadas por comas (al final un punto) ordenadas alfabéticamente, el número de habitantes que tiene cada una de ellas y su porcentaje respecto al total: </h3>
                <h5>Formato: AGUADULCE (Habitantes: 357, Porcentaje: 61.98%), ALBOX (Habitantes: 19, Porcentaje: 3.30%), BIE...</h5>
                
                <xsl:variable
                    name="totalHab"
                    select="count(/habitantes/habitante)">
                </xsl:variable>
                
                <xsl:key name="clave" match="habitante" use="localidad" />
                <xsl:for-each select="//habitante[generate-id(.) = generate-id(key('clave', localidad)[1])]">
                    <xsl:sort select="localidad"/>
                        <xsl:value-of select="localidad"/> 
                        <xsl:text> (Habitantes: </xsl:text>
                        <xsl:value-of select="count(key('clave', localidad))"/>                        
                        <xsl:text>, Porcentaje: </xsl:text>
                        <xsl:value-of select="format-number((count(key('clave', localidad)) * 100) div $totalHab, '0.00')" />
                        <xsl:text>%)</xsl:text>
                        <xsl:if test="position() != last()">, </xsl:if>
                        <xsl:if test="position() = last()">. </xsl:if>
                </xsl:for-each>
                
                <!--Sustituir por la sentencia xsl-->

                <h3>10. Crear una tabla, ordenada por orden alfabético de la columna nombre, con los habitantes de EL EJIDO mayores de 65 y los habitantes de ROQUETAS DE MAR menores de 20 años, con su cabecera "Nombre", "Apellidos", "Localidad" y "Edad". Cada localidad tendrá un color de fila diferente (azul para EL EJIDO y rojo para ROQUETAS DE MAR), siendo todos los textos en blanco y centrados. La cabecera tendrá un color de fondo negro. </h3>
                
                <table style="width:100%">
                    <thead style="background-color: #000;color:#fff; text-align:center">
                        <td>Nombre</td>
                        <td>Apellidos</td>
                        <td>Localidad</td>
                        <td>Edad</td>
                    </thead>
                    <xsl:for-each select="/habitantes/habitante">
                        <xsl:sort select="nombre"/>  
                        <xsl:choose>                          
                            <xsl:when test='(localidad="EL EJIDO" and edad &gt;65)'>
                                <tr style="background-color:#00F; color:#fff; text-align:center">
                                    <td>
                                        <xsl:value-of select="nombre"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="apellidos"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="localidad"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="edad"/>
                                    </td>
                                </tr>
                            </xsl:when>
                            <xsl:when test='(localidad ="ROQUETAS DE MAR" and edad&lt;20)'>
                                <tr style="background-color:#F00; color:#fff; text-align:center">
                                    <td>
                                        <xsl:value-of select="nombre"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="apellidos"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="localidad"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="edad"/>
                                    </td>
                                </tr>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:for-each>
                </table>
                <!--Sustituir por la sentencia xsl-->

            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
